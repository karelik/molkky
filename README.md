# Mölkky score sheet
Very simple javascript Mölkky score sheet that helps with coordinating game. It is public on [http://deskos.cz/molkky/](http://deskos.cz/molkky/).

It allows you to score by values predefined in side panels. Footer of table consists of sum of score and rest to 50, arrow shows team on turn. Warning appears if team has two misses in row. Score moves down to 25 when exceeds 50. Alert with winning team appears. You can go back (in case of mistake).

Enjoy Mölkky!

![screenshot](http://deskos.cz/molkky/img/screenshot.png)


 