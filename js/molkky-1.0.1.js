const TARGET_SCORE = 50
const FALL_SCORE = 25;
const MAX_SKITTLE = 12;

let points = [];
points[0] = []; // first team
points[0][0] = 0; // total set to 0
points[1] = []; // second team
points[1][0] = 0; // total set to 0

let turn = 1; // one turn means play of two teams
let onTurn = 0; // first team is 0, seconds is 1

// prepare table body
function createTbody(rows){
    for (let i = 1; i <= rows; i++) {
        addTbodyRow(i);
    }
}
function addTbodyRow(row){
    $('tbody').append('<tr>\n' +
        '<td class="uk-padding-remove-horizontal"></td>\n' +
        '<td class="uk-width-auto uk-padding-remove-horizontal">\n' +
        '<div class="uk-padding-remove-horizontal uk-grid-collapse uk-child-width-1-3" uk-grid>\n' +
        '<div class="uk-invisible">\n' +
        '<span uk-icon="triangle-left"></span>\n' +
        '</div>\n' +
        '<div>' + row + '.</div>\n' +
        '<div class="uk-invisible">\n' +
        '<span uk-icon="triangle-right"></span>\n' +
        '</div>\n' +
        '</div>\n' +
        '</td>\n' +
        '<td class="uk-padding-remove-horizontal"></td>\n' +
        '</tr>');
}
// create side control panel
function createNav(left) {
    const navClass = left ? 'uk-margin-right' : 'uk-margin-left';
    let nav = '<ul class="uk-nav uk-nav-primary ' + navClass + '">';
    for (let i = 12; i >= 0; i--){
        let classHere = '';
        let valStr = i.toString();
        if (i === 0){
            classHere = ' class="uk-text-danger"';
            valStr = 'X';
        }
        nav += '<li class="uk-text-center"><a href="javascript: add('+i+')"'+classHere+'>'+valStr+'</a></li>'
    }
    nav += '<li class="uk-text-center"><a href="javascript: back()"><span uk-icon="arrow-left"></span></a></li>';
    nav += '<li class="uk-text-center"><a href="javascript: newGame()"><span uk-icon="home"></span></a></li>';
    nav += '</ul>';
    return nav;
}
// add points to team is actual on turn
function add(val){
    points[onTurn][turn] = val; // history
    points[onTurn][0] += val; // total
    drawPoints();
    drawTotal();
    changeActualPlayingArrowsVisibility(false);
    next();
    drawNext();
}
// first player has left column/div, second right, so index is 1 or 3
function getIndex(){
    return onTurn === 0 ? 1 : 3;
}
// revert functionality, quite complex :(
function back(){
    if (turn === 1 && onTurn === 0) return;

    $('tbody tr:nth-child(' + turn + ') td:nth-child(' + getIndex() + ')').empty(); // cleans potential warning if present
    changeActualPlayingArrowsVisibility(false); // removes actual arrows
    prev(); // rewind one turn
    points[onTurn][turn] = 0; // reset internal score for turn
    recalculateTotal(); // revert total score internal
    drawTotal(); // make new total score visible
    $('tbody tr:nth-child(' + turn + ') td:nth-child(' + getIndex() + ')').empty(); // delete visible turn score
    // rewind and wait to new score
    prev()
    changeActualPlayingArrowsVisibility(false);
    next()
    drawNext();
}
// resets game
function newGame() {
    let safe = confirm('Opravdu?');
    if (safe) {
        // init again
        points = [];
        points[0] = [];
        points[0][0] = 0;
        points[1] = [];
        points[1][0] = 0;
        turn = 1;

        // clear tbody
        $('table tbody').empty();
        // recreate like during onload
        init();
    }
}
function recalculateTotal(){
    let sum = 0;
    for (let i = 1; i < points[onTurn].length; i++){
        sum += points[onTurn][i];
        if (sum > TARGET_SCORE) sum = FALL_SCORE;
    }
    points[onTurn][0] = sum;
}
// checks fall to 25 if total is over 50
function fallTo25(){
    if (points[onTurn][0] > TARGET_SCORE){
        points[onTurn][0] = FALL_SCORE;
        return true;
    }
    return false;
}
// draws points to table and checks game end
function drawPoints(){
    let text;
    if (points[onTurn][turn] === 0) {
        text = '<span class="uk-text-danger">X</span>';
        if (dangerFailure()){ // failure game end (3 misses in row)
            alert('Hra skončila! Vyhrál '+ (onTurn === 1 ? 'Tým 1' : 'Tým 2'));
        }
    } else {
        if (fallTo25()){
            text = '<span class="uk-label uk-label-danger">' + points[onTurn][turn] + '</span>';
        } else if (points[onTurn][0] === 50) { // success game end (reaching 50)
            text = '<span class="uk-label uk-label-success">' + points[onTurn][turn] + '</span>';
            alert('Hra skončila! Vyhrál ' + (onTurn === 0 ? 'Tým 1' : 'Tým 2'));
        } else {
            text = points[onTurn][turn].toString();
        }
    }
    $('tbody tr:nth-child(' + turn + ') td:nth-child(' + getIndex() + ')').html(text);
}
// draws total to summary row
function drawTotal(){
    const rest = TARGET_SCORE - points[onTurn][0];
    const totalText = '<span class="">' + points[onTurn][0] + '</span><br><span class="uk-label uk-label-success">' + rest + '</span>';
    $('tfoot td:nth-child(' + getIndex() + ')').html(totalText)
}
// adds or removes old symbols to show who plays
function changeActualPlayingArrowsVisibility(visible) {
    const divIndex = getIndex();
    const tags = $(
        'tbody tr:nth-child(' + turn + ') td:nth-child(2) div div:nth-child(' + divIndex + ')' +
        ', tfoot td:nth-child(2) div div:nth-child(' + divIndex + ')'
    );
    if (visible){
        tags.removeClass('uk-invisible')
    } else {
        tags.addClass('uk-invisible');
    }
}
// draws new symbols to show who plays
function drawNext() {
    if ($('tbody tr').length < turn){
        addTbodyRow(turn);
    }
    changeActualPlayingArrowsVisibility(true);
    const dangerDiv = $('tfoot td:nth-child(2) div div:nth-child(2)');
    const dangerDivSpan = $('tfoot td:nth-child(2) div div:nth-child(2) span');
    let text;
    if (dangerFailure()){
        text = '<span class="blink_me uk-text-danger uk-text-bold ">!</span>';
        $('tbody tr:nth-child(' + turn + ') td:nth-child(' + getIndex() + ')').html(text);
        dangerDivSpan.removeClass('uk-text-warning').addClass('uk-text-danger');
        dangerDiv.removeClass('uk-invisible');
    } else if (dangerPrecise()){
        text = '<span class="blink_me uk-text-warning uk-text-bold ">!</span>';
        $('tbody tr:nth-child(' + turn + ') td:nth-child(' + getIndex() + ')').html(text);
        dangerDivSpan.removeClass('uk-text-danger').addClass('uk-text-warning');
        dangerDiv.removeClass('uk-invisible');
    } else {
        dangerDiv.addClass('uk-invisible');
    }
}
// checks if actual team is in danger of fall down to 25 if hits wrong skittle
function dangerPrecise() {
    return points[onTurn][0] > (TARGET_SCORE - MAX_SKITTLE);
}
// checks if actual team is in danger of failure if misses now (3 misses in row)
function dangerFailure() {
    if (turn > 2) {
        for (let i = 1; i < 3; i++) {
            if (points[onTurn][turn - i] !== 0) return false;
        }
        return true;
    }
    return false;
}
// moves control variables to next
function next(){
    if (onTurn === 0) onTurn++;
    else {
        onTurn = 0;
        turn++;
    }
}
// moves control variables to previous
function prev(){
    if (onTurn === 1) onTurn--;
    else {
        onTurn = 1;
        turn--;
    }
}
// draws navigation, triggered once
function drawNavigation(){
    $('#navBefore').append(createNav(true));
    $('#navAfter').append(createNav(false));
}
// inits table rows, totals and prepares first team begin
function init(){
    createTbody(12);
    onTurn = 0;
    drawTotal()
    onTurn = 1;
    drawTotal();
    onTurn = 0;
    drawNext();
}
// activates event to fill team name
function activateTeamNamePrompt() {
    $('.teamName').on('click', function(){
        let teamName = prompt('Zadej jméno týmu', $(this).html());
        $(this).html(teamName);
    });
}
$(document).ready(function () {
    drawNavigation();
    activateTeamNamePrompt();
    init();
})